<?php
$grn = 27.84;
$tours = [
[
    'title' => 'South America',
    'price' => 2500,
    'country' => ['Argentina', 'Brasil', 'Peru'],
    'date' => '26.03.2021'
],
[
    'title' => 'North America',
    'price' => 2800,
    'country' => ['USA', 'Canada', 'Mexico'],
    'date' => '2.04.2021'
],
[
    'title' => 'Grand Canyon',
    'price' => 2000,
    'country' => ['USA'],
    'date' => '22.04.2021'
],
[
    'title' => 'Карибский басейн',
    'price' => 4500,
    'country' => ['Куба', 'Ямайка','Панама'],
    'date' => '30.05.2021'
],
[
    'title' => 'Прага',
    'price' => 300,
    'country' => ['Чехия'],
    'date' => '8.06.2021'
],
[
    'title' => 'Париж',
    'price' => 700,
    'country' =>['Франция'],
    'date' => '16.06.2021'
],
[
    'title' => 'Берлин',
    'price' => 600,
    'country' => ['Германия'],
    'date' => '29.06.2021'
],
[
    'title' => 'Цюрих',
    'price' => 800,
    'country' => ['Швейцария'],
    'date' => '10.07.2021'
],
[
    'title' => 'Вена',
    'price' => 1200,
    'country' => ['Австрия'],
    'date' => '21.07.2021'
],
[
    'title' => 'Адис-Абеба',
    'price' => 6300,
    'country' => ['Эфиопия'],
    'date' => '22.01.2022'
],
[
    'title' => 'West Europe',
    'price' => 3700,
    'country' => ['Португалия','Испания','Андорра'],
    'date' => '18.07.2021'
]
];
echo 'few commits';
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Description" content="table with numerals array">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>HW_7_Samsonnykov</title>
</head>
<body>
    <h1 class="text-center">Наши туры</h1>
    <div>
        <table class="table table-hover">
            <thead class="table-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Цена в $</th>
                    <th scope="col">Цена в ₴</th>
                    <th scope="col">Страна</th>
                    <th scope="col">Дата</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tours as $key => $tour) : ?>
                    <tr>
                        <th scope="row"><?= $key++;?></th>
                        <td><?= $tour['title']; ?></td>
                        <td><?= $tour['price']; ?></td>
                        <td><?= $grn * $tour['price']; ?></td>
                        <td>
                            <?php foreach($tour['country'] as $country) : ?>
                                <ul>
                                    <li>
                                        <?= $country ; ?>
                                    </li>
                                </ul>
                            <?php endforeach; ?>
                        </td>
                        <td><?= $tour['date']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>    
</body>
</html>